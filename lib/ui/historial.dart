import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lovix/bloc/profile/profile_bloc.dart';
import 'package:lovix/models/profile.dart';
import 'package:lovix/ui/custom/avatar.dart';
import 'package:lovix/ui/custom/my_card.dart';
import 'package:lovix/utils/user_utitlity.dart';

class HistorialPage extends StatelessWidget {
  List<Widget> _mapCallsToCallViews(List<Call> calls) {
    return calls.map((e) => _callView(e)).toList();
  }

  Widget _callView(Call call) {
    return MyCard(
      child: FutureBuilder<Profile>(
          future: UserUtility.getUser(call.otherUid),
          builder: (context, snapshot) {
            return snapshot.hasData
                ? Row(
                    children: [
                      AvatarView(
                        profile: snapshot.data,
                        size: 28,
                      ),
                      SizedBox(
                        width: 18,
                      ),
                      Column(
                        children: [
                          Text(snapshot.data.name),
                          Text(call.start.toIso8601String()),
                        ],
                      ),
                    ],
                  )
                : Center(
                    child: CircularProgressIndicator(),
                  );
          }),
    );
  }

  @override
  Widget build(BuildContext context) {
    List<Call> calls =
        BlocProvider.of<ProfileBloc>(context).state.profile.history;
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: Colors.white10,
      padding: EdgeInsets.only(
        left: 10,
        right: 10,
        top: 6,
      ),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            if (calls.length == 0) Text('No call history'),
            ..._mapCallsToCallViews(calls),
          ],
        ),
      ),
    );
  }
}
