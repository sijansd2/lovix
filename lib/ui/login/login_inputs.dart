import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:lovix/bloc/login/login_bloc.dart';
import 'package:lovix/bloc/login/login_event.dart';

class LoginInputs extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  LoginInputs({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // ignore: close_sinks
    final _loginBloc = BlocProvider.of<LoginBloc>(context);
    final _numberController = TextEditingController();
    var _phoneNumber = PhoneNumber(isoCode: 'US');
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            // SizedBox(
            //   height: 32.0,
            // ),
            Container(
              height: 300,
              width: double.infinity,
              color: Colors.orange,
              child: Image.asset(
                'assets/login/splash_image.png',
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 32.0, horizontal: 55.0),
              child: Column(
                children: [
                  InternationalPhoneNumberInput(
                    textFieldController: _numberController,
                    initialValue: _phoneNumber,
                    onInputChanged: (value) {
                      _phoneNumber = value;
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  // SizedBox(
                  //   height: 12.0,
                  // ),
                  // SizedBox(
                  //   height: 32.0,
                  // ),
                  // SizedBox(
                  //   height: 32.0,
                  // ),
                  // Card(
                  //   child: ListTile(
                  //     leading: Icon(FontAwesomeIcons.sms),
                  //     title: Text('SMS'),
                  //     onTap: () {
                  //       if (_formKey.currentState.validate())
                  //         _loginBloc.add(
                  //           LoginEvent(
                  //             LoginAction.signWithPhone,
                  //             data: _phoneNumber.dialCode + _numberController.text,
                  //           ),
                  //         );
                  //     },
                  //   ),
                  // ),
                  // InkWell(
                  //   onTap: () {},
                  //   child: Container(
                  //     height: 40.0,
                  //     width: double.infinity,
                  //     padding: EdgeInsets.symmetric(
                  //       horizontal: 16.0,
                  //     ),
                  //     decoration: BoxDecoration(
                  //       color: Colors.blue,
                  //       borderRadius: BorderRadius.circular(26),
                  //     ),
                  //     child: Row(
                  //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  //       children: [
                  //         Icon(
                  //           FontAwesomeIcons.sms,
                  //           color: Colors.white,
                  //           size: 26,
                  //         ),
                  //         Spacer(
                  //           flex: 1,
                  //         ),
                  //         SizedBox(
                  //           height: 30.0,
                  //           child: VerticalDivider(
                  //             color: Colors.white,
                  //             thickness: 1,
                  //           ),
                  //         ),
                  //         Spacer(
                  //           flex: 3,
                  //         ),
                  //         Text(
                  //           'Login With SMS',
                  //           style: TextStyle(color: Colors.white),
                  //         ),
                  //         Spacer(
                  //           flex: 15,
                  //         )
                  //       ],
                  //     ),
                  //   ),
                  // ),
                  // Card(
                  //   child: ListTile(
                  //     leading: Icon(FontAwesomeIcons.google),
                  //     title: Text('Google'),
                  //     onTap: () {
                  //       _loginBloc.add(
                  //         LoginEvent(LoginAction.signWithGoogle),
                  //       );
                  //     },
                  //   ),
                  // ),
                  // Card(
                  //   child: ListTile(
                  //     leading: Icon(FontAwesomeIcons.facebookF),
                  //     title: Text('Facebook'),
                  //     onTap: () {
                  //       _loginBloc.add(
                  //         LoginEvent(LoginAction.signInWithFacebook),
                  //       );
                  //     },
                  //   ),
                  // ),
                  InkWell(
                    onTap: () {
                      if (_formKey.currentState.validate())
                        _loginBloc.add(
                          LoginEvent(
                            LoginAction.signWithPhone,
                            data:
                                _phoneNumber.dialCode + _numberController.text,
                          ),
                        );
                    },
                    child: Image.asset('assets/login/sms_btn.png'),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    width: double.infinity,
                    child: Row(
                      children: [
                        Expanded(
                          child: Divider(
                            thickness: 1,
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          'OR',
                          style: TextStyle(color: Colors.grey),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Expanded(
                          child: Divider(
                            thickness: 1,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  InkWell(
                    onTap: () {
                      _loginBloc.add(
                        LoginEvent(LoginAction.signInWithFacebook),
                      );
                    },
                    child: Image.asset('assets/login/fb_btn.png'),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  InkWell(
                    onTap: () {
                      _loginBloc.add(
                        LoginEvent(LoginAction.signWithGoogle),
                      );
                    },
                    child: Image.asset('assets/login/google_btn.png'),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
