import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:lovix/bloc/login/login_bloc.dart';
import 'package:lovix/bloc/login/login_event.dart';
import 'package:lovix/bloc/login/login_state.dart' as LoginState;
import 'package:lovix/ui/login/login_inputs.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // ignore: close_sinks
    final LoginBloc _loginBloc = BlocProvider.of<LoginBloc>(context);
    return SafeArea(
      child: Scaffold(
        body: Container(
          decoration: BoxDecoration(
              color: Theme.of(context).scaffoldBackgroundColor,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(16.0),
                  topRight: Radius.circular(16.0))),
          // child: Padding(
          //   padding: const EdgeInsets.only(
          //       right: 32.0, left: 32.0, top: 16.0, bottom: 32.0),
          child: BlocBuilder<LoginBloc, LoginState.LoginState>(
            bloc: _loginBloc,
            builder: (context, state) {
              switch (state.runtimeType) {
                case LoginState.Initial:
                  return LoginInputs();
                case LoginState.Eror:
                  return Container(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Icon(FontAwesomeIcons.checkCircle),
                        SelectableText(
                          (state as LoginState.Eror).error ?? 'Unknown Error',
                        ),
                        RaisedButton(
                          onPressed: () {
                            _loginBloc.add(
                              LoginEvent(LoginAction.dismissMessage),
                            );
                          },
                          child: Text('OK'),
                        )
                      ],
                    ),
                  );
                case LoginState.NeedCode:
                  final codeController = TextEditingController();
                  return Center(
                    child: Column(
                      children: [
                        TextFormField(
                          controller: codeController,
                          decoration:
                              InputDecoration(hintText: 'Enter Code Here'),
                        ),
                        RaisedButton(
                          onPressed: () {
                            _loginBloc.add(
                              LoginEvent(
                                LoginAction.verifyPhone,
                                data: codeController.text,
                                verId: (state as LoginState.NeedCode).verId,
                              ),
                            );
                          },
                          child: Text('OK'),
                        )
                      ],
                    ),
                  );
                case LoginState.Loading:
                default:
                  return Container(
                    height: 200.0,
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
              }
            },
          ),
        ),
      ),
      // ),
    );
  }
}
