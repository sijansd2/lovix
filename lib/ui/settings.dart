import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lovix/bloc/home/home_bloc.dart';
import 'package:lovix/bloc/home/home_event.dart';
import 'package:lovix/bloc/login/login_bloc.dart';
import 'package:lovix/bloc/login/login_event.dart';
import 'package:lovix/bloc/profile/profile_bloc.dart';
import 'package:lovix/ui/confirm_form.dart';
import 'package:lovix/ui/custom/avatar.dart';
import 'package:lovix/ui/custom/my_card.dart';
import 'package:lovix/utils/shared_prefs.dart';

class Settings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // ignore: close_sinks
    final _profileBloc = BlocProvider.of<ProfileBloc>(context);
    // ignore: close_sinks
    final _loginBloc = BlocProvider.of<LoginBloc>(context);
    // ignore: close_sinks
    final _homeBloc = BlocProvider.of<HomeBloc>(context);
    final profile = _profileBloc.state.profile;
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: Colors.white10,
      padding: EdgeInsets.only(
        left: 10,
        right: 10,
        top: 6,
      ),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            MyCard(
              child: Row(
                children: [
                  AvatarView(
                    onTap: () {
                      _homeBloc.add(OpenProfile());
                    },
                    profile: profile,
                    size: 28,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        profile.name ?? 's',
                        style: TextStyle(
                          fontSize: 15,
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      //Text(profile.uid),
                    ],
                  ),
                  Spacer(),
                  IconButton(
                      icon: Icon(Icons.logout),
                      onPressed: () {
                        _loginBloc.add(LogoutEvent());
                      }),
                  SizedBox(
                    width: 15,
                  ),
                ],
              ),
            ),
            NotificationSwitch(),
            // SwitchSettings(
            //   title: 'Dark Mode',
            // ),
            MyCard(
              onTap: () {
                _homeBloc.add(OpenCoins());
              },
              child: Text(
                'Credits',
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class NotificationSwitch extends StatefulWidget {
  @override
  _NotificationSwitchState createState() => _NotificationSwitchState();
}

class _NotificationSwitchState extends State<NotificationSwitch> {
  bool checked = false;

  @override
  void initState() {
    super.initState();
    SharedPrefsUtility.getString(SharedPrefsUtility.FCM_TOKEN).then((value) {
      if (value != null && value != '') {
        checked = true;
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SwitchSettings(
      title: 'Notification',
      checked: checked,
      onChange: (val) {
        if (val) {
          showDialog(
            context: context,
            builder: (context) {
              return ConfirmForm(
                message: 'Are you sure you want to enable notifications?',
                confirmText: 'OK',
                cancelText: 'Cancel',
              );
            },
          ).then((value) {
            if (value == ConfirmForm.agree) {
              FirebaseMessaging()
                ..getToken().then((token) {
                  SharedPrefsUtility.saveString(
                      SharedPrefsUtility.FCM_TOKEN, token);
                });
              setState(() {
                checked = true;
              });
            }
          });
        } else {
          SharedPrefsUtility.saveString(SharedPrefsUtility.FCM_TOKEN, '');
          setState(() {
            checked = false;
          });
        }
      },
    );
  }
}

class SwitchSettings extends StatelessWidget {
  final String title;
  final Function onChange;
  final bool checked;
  const SwitchSettings({
    Key key,
    this.title = '',
    this.onChange,
    this.checked = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MyCard(
      child: Row(
        children: [
          Text(
            title,
            style: TextStyle(
              fontSize: 15,
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          Spacer(),
          Switch(
            value: checked,
            onChanged: onChange,
          ),
        ],
      ),
    );
  }
}
