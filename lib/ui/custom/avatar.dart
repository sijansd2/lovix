import 'package:flutter/material.dart';
import 'package:lovix/models/profile.dart';

class AvatarView extends StatelessWidget {
  AvatarView({
    Key key,
    @required this.profile,
    this.size = 50,
    this.onTap,
  }) : super(key: key);

  final Profile profile;
  final double size;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.only(right: 1, bottom: 1),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
        ),
        child: CircleAvatar(
          radius: size,
          backgroundImage: profile.photoUrl == null
              ? AssetImage('assets/avatar_placeholder.png')
              : NetworkImage(profile.photoUrl),
        ),
      ),
    );
  }
}
