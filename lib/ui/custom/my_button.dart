import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  final Function onPressed;

  final IconData icon;

  final String text;

  const MyButton(
      {Key key,
      @required this.onPressed,
      @required this.icon,
      @required this.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton.icon(
      textColor: Colors.white,
      color: Color(0xFF6200EE),
      onPressed: onPressed,
      icon: Icon(icon, size: 18),
      label: Text(text),
    );
  }
}
