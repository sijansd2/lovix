import 'package:flutter/material.dart';

class MyCard extends StatelessWidget {
  final Widget child;
  final Function onTap;
  const MyCard({Key key, this.child, this.onTap}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Card(
        color: Colors.white,
        child: Container(
          padding: EdgeInsets.only(
            left: 10,
            right: 10,
            top: 20,
            bottom: 20,
          ),
          width: double.infinity,
          child: child,
        ),
      ),
    );
  }
}
