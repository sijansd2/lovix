import 'package:flutter/material.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';

class CallView extends StatelessWidget {
  const CallView({
    Key key,
    @required RTCVideoRenderer remoteRenderer,
    @required RTCVideoRenderer localRenderer,
  })  : _remoteRenderer = remoteRenderer,
        _localRenderer = localRenderer,
        super(key: key);

  final RTCVideoRenderer _remoteRenderer;
  final RTCVideoRenderer _localRenderer;

  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(builder: (context, orientation) {
      return Container(
        child: Stack(children: <Widget>[
          Positioned(
              left: 0.0,
              right: 0.0,
              top: 0.0,
              bottom: 0.0,
              child: Container(
                margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: RTCVideoView(_remoteRenderer),
                decoration: BoxDecoration(color: Colors.black54),
              )),
          Positioned(
            left: 20.0,
            top: 20.0,
            child: Container(
              width: orientation == Orientation.portrait ? 90.0 : 120.0,
              height: orientation == Orientation.portrait ? 120.0 : 90.0,
              child: RTCVideoView(_localRenderer),
              decoration: BoxDecoration(color: Colors.black54),
            ),
          ),
        ]),
      );
    });
  }
}
