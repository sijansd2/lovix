import 'package:flutter/material.dart';
import 'package:lovix/models/coin_package.dart';
import 'package:lovix/services/coins_service.dart';
import 'package:lovix/utils/service_locator.dart';

class CoinsPage extends StatelessWidget {
  final CoinsService _coinsService = locator<CoinsService>();
  List<Widget> _getPackagesView(List<CoinPackage> items) {
    return items.map((e) {
      return Container(
        margin: EdgeInsets.only(
          left: 40,
          right: 40,
          top: 20,
        ),
        padding: EdgeInsets.only(
          left: 20,
          right: 20,
          top: 10,
          bottom: 10,
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
        ),
        child: Row(
          children: [
            Text(e.amount.toInt().toString()),
            Spacer(),
            Text(e.price.toString()),
          ],
        ),
      );
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        children: [
          CoinsHeader(),
          Expanded(
            child: SingleChildScrollView(
              child: FutureBuilder<List<CoinPackage>>(
                  future: _coinsService.getPackages(),
                  initialData: <CoinPackage>[],
                  builder: (context, snapshot) {
                    print(snapshot.data);
                    return Column(
                      children: [
                        ..._getPackagesView(snapshot.data),
                      ],
                    );
                  }),
            ),
          ),
          CoinsConfirm(),
        ],
      ),
    );
  }
}

class CoinsConfirm extends StatelessWidget {
  const CoinsConfirm({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        print('buy coins');
      },
      child: Container(
        margin: EdgeInsets.only(
          bottom: 20,
        ),
        height: 45,
        width: 260,
        decoration: BoxDecoration(
          color: Colors.orange,
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
        ),
        child: Center(
          child: Text(
            'Continue',
            style: TextStyle(
              fontSize: 20,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}

class CoinsHeader extends StatelessWidget {
  const CoinsHeader({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: Colors.orange,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            height: 25,
          ),
          Text(
            'My Coins',
            style: TextStyle(
              fontSize: 20,
              color: Colors.white,
            ),
          ),
          SizedBox(
            height: 25,
          ),
          Container(
            height: 40,
            width: 160,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(20),
              ),
            ),
            child: Center(
              child: RichText(
                text: TextSpan(
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 24,
                  ),
                  children: [
                    WidgetSpan(
                      child: Icon(
                        Icons.attach_money_rounded,
                        size: 24,
                      ),
                    ),
                    TextSpan(text: '9'),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(
            height: 25,
          ),
          Text(
            'Buy more coins to be able to connect more',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
            ),
          ),
          SizedBox(
            height: 25,
          ),
        ],
      ),
    );
  }
}
