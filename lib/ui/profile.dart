import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lovix/bloc/profile/profile_bloc.dart';
import 'package:lovix/bloc/profile/profile_event.dart';
import 'package:lovix/bloc/profile/profile_state.dart';
import 'package:lovix/models/profile.dart';
import 'package:lovix/ui/custom/avatar.dart';
import 'package:lovix/ui/custom/my_button.dart';
import 'package:lovix/utils/storage_utility.dart';

class ProfilePage extends StatelessWidget {
  final _nameController = TextEditingController();
  final _aboutController = TextEditingController();
  final _ageController = TextEditingController();
  final _jobController = TextEditingController();

  Future<File> _getImage() async {
    final ImagePicker _imagePicker = ImagePicker();
    final PickedFile pickedFile =
        await _imagePicker.getImage(source: ImageSource.gallery);
    return File(pickedFile.path);
  }

  @override
  Widget build(BuildContext context) {
    // ignore: close_sinks
    final _profileBloc = BlocProvider.of<ProfileBloc>(context);
    return BlocBuilder<ProfileBloc, ProfileState>(
      bloc: _profileBloc,
      builder: (context, state) {
        bool needSave = false;
        bool loadingPic = false;
        Profile p = state.profile;
        if (!p.loaded) {
          _profileBloc.add(ProfileEvent(p));
        }
        _nameController.text = p.name;
        _aboutController.text = p.about;
        _ageController.text = p.age.toString();
        _jobController.text = p.job;
        return Container(
          width: double.infinity,
          margin: EdgeInsets.only(
            left: 10,
            right: 10,
          ),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 20,
                ),
                if (p.saving == SavingState.saving) CircularProgressIndicator(),
                Stack(
                  children: [
                    AvatarView(profile: p),
                    Positioned(
                      bottom: 0,
                      right: 0,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                        ),
                        child: IconButton(
                            onPressed: () {
                              _getImage().then((file) {
                                StorageUtility.upload(p.uid, file).then((_) {
                                  StorageUtility.getImage(p.uid).then((url) {
                                    p.photoUrl = url;
                                    _profileBloc.add(ProfileEvent(p));
                                  });
                                });
                              });
                            },
                            icon: Icon(Icons.edit)),
                      ),
                    ),
                    if (loadingPic)
                      Positioned(
                        top: 25,
                        right: 25,
                        child: CircularProgressIndicator(),
                      ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: _nameController,
                  onChanged: (val) {
                    needSave = true;
                  },
                  decoration: InputDecoration(
                    labelText: 'name',
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: _aboutController,
                  onChanged: (val) {
                    needSave = true;
                  },
                  minLines: 4,
                  maxLines: 4,
                  decoration: InputDecoration(
                    labelText: 'about me',
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: _ageController,
                  onChanged: (val) {
                    needSave = true;
                  },
                  validator: (val) {
                    try {
                      int.parse(val);
                    } on FormatException {
                      return 'Invalid Age value';
                    }
                    return '';
                  },
                  decoration: InputDecoration(
                    labelText: 'age',
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: _jobController,
                  onChanged: (val) {
                    needSave = true;
                  },
                  decoration: InputDecoration(
                    labelText: 'job',
                  ),
                ),
                MyButton(
                  onPressed: () {
                    if (needSave) {
                      p.name = _nameController.text;
                      p.about = _aboutController.text;
                      p.age = int.parse(_ageController.text);
                      p.job = _jobController.text;
                      _profileBloc.add(ProfileEvent(p));
                    } else {
                      _showMessageDialog(context);
                    }
                  },
                  text: 'Save',
                  icon: Icons.save,
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Future _showMessageDialog(BuildContext context,
      {String content = 'changes saved successfully', String title = 'Done'}) {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(title),
          content: Text(content),
          actions: [
            MyButton(
              onPressed: () {
                Navigator.pop(context);
              },
              text: 'OK',
              icon: Icons.done,
            )
          ],
        );
      },
    );
  }
}
