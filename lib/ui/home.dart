import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:lovix/bloc/home/home_bloc.dart';
import 'package:lovix/bloc/home/home_event.dart';
import 'package:lovix/bloc/home/home_state.dart' as HomeState;
import 'package:lovix/bloc/profile/profile_bloc.dart';
import 'package:lovix/bloc/profile/profile_event.dart';
import 'package:lovix/ui/coins.dart';
import 'package:lovix/ui/composed/home_view.dart';
import 'package:lovix/ui/historial.dart';
import 'package:lovix/ui/profile.dart';
import 'package:lovix/ui/settings.dart';

class HomePage extends StatelessWidget {
  // Shown
  static const int HISTORIAL_INDEX = 0;
  static const int HOME_INDEX = 1;
  static const int SETTINGS_INDEX = 2;

  // Not Shown
  static const int PROFILE_INDEX = 2;
  static const int COINS_INDEX = 2;

  Widget getBody(state) {
    switch (state.runtimeType) {
      case HomeState.Profile:
        return ProfilePage();
      case HomeState.Coins:
        return CoinsPage();
      case HomeState.SettingsState:
        return Settings();
      case HomeState.HistorialState:
        return HistorialPage();
      case HomeState.Initial:
      default:
        return HomeView();
    }
  }

  @override
  Widget build(BuildContext context) {
    // ignore: close_sinks
    final _homeBloc = BlocProvider.of<HomeBloc>(context);
    // ignore: close_sinks
    final _profileBloc = BlocProvider.of<ProfileBloc>(context);
    if (!_profileBloc.state.profile.loaded)
      _profileBloc.add(ProfileEvent(_profileBloc.state.profile));

    void _onItemTapped(int index) {
      HomeEvent e;
      switch (index) {
        case HISTORIAL_INDEX:
          e = OpenHistorial();
          break;
        case SETTINGS_INDEX:
          e = OpenSettings();
          break;
        case PROFILE_INDEX:
          e = OpenProfile();
          break;
        case COINS_INDEX:
          e = OpenCoins();
          break;
        case HOME_INDEX:
        default:
          e = OpenInitial();
          break;
      }
      _homeBloc.add(e);
    }

    return BlocBuilder<HomeBloc, HomeState.HomeState>(
        bloc: _homeBloc,
        builder: (context, state) {
          int _selectedIndex = 0;
          switch (state.runtimeType) {
            case HomeState.Profile:
              _selectedIndex = PROFILE_INDEX;
              break;
            case HomeState.Coins:
              _selectedIndex = COINS_INDEX;
              break;
            case HomeState.HistorialState:
              _selectedIndex = HISTORIAL_INDEX;
              break;
            case HomeState.SettingsState:
              _selectedIndex = SETTINGS_INDEX;
              break;
            case HomeState.HomeState:
            case HomeState.Initial:
            default:
              _selectedIndex = HOME_INDEX;
              break;
          }
          return SafeArea(
            child: Scaffold(
              // appBar: AppBar(
              //   title: Text('Lovix'),
              //   iconTheme: new IconThemeData(color: Colors.green),
              // ),
              // drawer: MyDrawer(homeBloc: _homeBloc),
              bottomNavigationBar: BottomNavigationBar(
                items: [
                  BottomNavigationBarItem(
                    icon: Icon(FontAwesomeIcons.fireAlt),
                    label: '',
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(FontAwesomeIcons.home),
                    label: '',
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(FontAwesomeIcons.userAlt),
                    label: '',
                  ),
                ],
                currentIndex: _selectedIndex,
                onTap: _onItemTapped,
              ),
              body: getBody(state),
            ),
          );
        });
  }
}

class MyDrawer extends StatelessWidget {
  const MyDrawer({
    Key key,
    @required HomeBloc homeBloc,
    this.state,
  })  : _homeBloc = homeBloc,
        super(key: key);

  final HomeBloc _homeBloc;
  final HomeState.HomeState state;

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          ListTile(
            leading: Icon(Icons.home),
            title: Text('Home'),
            selected: state is HomeState.Initial,
            onTap: () {
              Navigator.pop(context);
              _homeBloc.add(OpenInitial());
            },
          ),
          ListTile(
            leading: Icon(Icons.supervised_user_circle),
            title: Text('Profile'),
            selected: state is HomeState.Profile,
            onTap: () {
              Navigator.pop(context);
              _homeBloc.add(OpenProfile());
            },
          ),
          ListTile(
            leading: Icon(Icons.money),
            title: Text('Coins'),
            selected: state is HomeState.Coins,
            onTap: () {
              Navigator.pop(context);
              _homeBloc.add(OpenCoins());
            },
          ),
        ],
      ),
    );
  }
}
