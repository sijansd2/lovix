import 'package:flutter/material.dart';
import 'package:lovix/ui/call.dart';
import 'package:lovix/ui/composed/mins_dialog.dart';
import 'package:lovix/ui/custom/my_button.dart';
import 'package:lovix/ui/composed/filter_dialog.dart';

class HomeView extends StatelessWidget {
  Widget _buildHeader() {
    return Text(
      'LOVIX',
      style: TextStyle(
        fontSize: 28,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  Widget _buildButtonRow(context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        InkWell(
          onTap: () {
            showDialog(
              context: context,
              builder: (context) {
                return FilterDialog();
              },
            );
          },
          borderRadius: BorderRadius.circular(20),
          child: Image.asset('assets/home/filter.png'),
        ),
        InkWell(
          onTap: () {
            showDialog(
              context: context,
              builder: (context) {
                return MinsDialog();
              },
            );
          },
          borderRadius: BorderRadius.circular(20),
          child: Image.asset('assets/home/mins_btn.png'),
        ),
      ],
    );
  }

  Widget _buildHeaderAndButtonRow(context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 30,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          _buildHeader(),
          _buildButtonRow(context),
        ],
      ),
    );
  }

  Widget _buildCallButton(context) {
    return Center(
      child: InkWell(
        onTap: () {
          Navigator.pushNamed(context, CallPage.ROUTE);
        },
        borderRadius: BorderRadius.circular(40),
        child: Image.asset('assets/home/call_icon.png'),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // return Center(
    //   child: Column(
    //     mainAxisSize: MainAxisSize.min,
    //     children: [
    //       Row(
    //         mainAxisSize: MainAxisSize.min,
    //         children: [
    //           MyButton(
    //             onPressed: () {
    //               showDialog(
    //                 context: context,
    //                 builder: (context) {
    //                   return FilterDialog();
    //                 },
    //               );
    //             },
    //             icon: Icons.filter_1,
    //             text: 'Filter',
    //           ),
    //           SizedBox(
    //             width: 8,
    //           ),
    //           MyButton(
    //             onPressed: () {
    //               showDialog(
    //                 context: context,
    //                 builder: (context) {
    //                   return MinsDialog();
    //                 },
    //               );
    //             },
    //             icon: Icons.timelapse,
    //             text: '+Mins',
    //           ),
    //         ],
    //       ),
    //       SizedBox(
    //         height: 50,
    //       ),
    //       // Ink(
    //       //   height: 80,
    //       //   width: 80,
    //       //   decoration: const ShapeDecoration(
    //       //     color: Colors.lightBlue,
    //       //     shape: CircleBorder(),
    //       //   ),
    //       //   child: IconButton(
    //       //     icon: Icon(
    //       //       Icons.call,
    //       //       size: 40,
    //       //     ),
    //       //     onPressed: () {
    //       //       Navigator.pushNamed(context, CallPage.ROUTE);
    //       //     },
    //       //   ),
    //       // ),
    //     ],
    //   ),
    // );
    return Column(
      children: [
        Expanded(
          flex: 5,
          child: _buildHeaderAndButtonRow(context),
        ),
        Spacer(
          flex: 5,
        ),
        Expanded(
          flex: 6,
          child: _buildCallButton(context),
        ),
      ],
    );
  }
}
