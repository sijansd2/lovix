import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_picker_dropdown.dart';
import 'package:country_pickers/country_pickers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lovix/bloc/filter/filter_bloc.dart';
import 'package:lovix/bloc/filter/filter_event.dart';
import 'package:lovix/bloc/filter/filter_state.dart';
import 'package:lovix/models/filter.dart';
import 'package:lovix/ui/custom/my_button.dart';

class FilterDialog extends StatelessWidget {
  Widget _getDialogView(FilterCurrentState state, FilterBloc _bloc, Filter f,
      BuildContext context) {
    switch (state) {
      case FilterCurrentState.loaded:
      case FilterCurrentState.changed:
        return FilterInputs(
          bloc: _bloc,
          f: f,
        );
      case FilterCurrentState.initial:
        return SizedBox(
          height: 200,
          width: 200,
          child: Center(child: CircularProgressIndicator()),
        );
        break;
      case FilterCurrentState.saved:
      default:
        return CallAfterWhile(
          toCall: () {
            Navigator.pop(context);
          },
          child: SizedBox(
            height: 200,
            width: 200,
            child: Center(child: CircularProgressIndicator()),
          ),
        );
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    // ignore: close_sinks
    FilterBloc _bloc = BlocProvider.of<FilterBloc>(context);
    return BlocBuilder<FilterBloc, FilterState>(
        bloc: _bloc,
        builder: (context, state) {
          Filter f = state.filter;

          if (state.state == FilterCurrentState.initial)
            _bloc.add(FilterEvent(f, FilterAction.load));

          return Dialog(
            child: Padding(
              padding: EdgeInsets.all(10),
              child: SizedBox(
                width: 350,
                child: _getDialogView(state.state, _bloc, f, context),
              ),
            ),
          );
        });
  }
}

class CallAfterWhile extends StatelessWidget {
  final Function toCall;
  final Widget child;

  const CallAfterWhile({Key key, this.toCall, this.child}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration(seconds: 1)).then((value) => toCall());
    return child;
  }
}

class FilterInputs extends StatelessWidget {
  final FilterBloc bloc;
  final Filter f;

  Widget _buildDropdownItem(Country country) => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          CountryPickerUtils.getDefaultFlagImage(country),
          SizedBox(
            width: 8.0,
          ),
          Text(country.name),
        ],
      );

  const FilterInputs({Key key, @required this.bloc, @required this.f})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    RangeValues currentRangeValues = RangeValues(f.minAge, f.maxAge);
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text('Gender'),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Radio(
              value: Filter.MALE,
              groupValue: f.gender,
              onChanged: (val) {
                f.gender = val;
                bloc.add(FilterEvent(f, FilterAction.edit));
              },
            ),
            Text('Male'),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Radio(
              value: Filter.FEMALE,
              groupValue: f.gender,
              onChanged: (val) {
                f.gender = val;
                bloc.add(FilterEvent(f, FilterAction.edit));
              },
            ),
            Text('Female'),
          ],
        ),
        Text('Age'),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(currentRangeValues.start.round().toString()),
            RangeSlider(
              values: currentRangeValues,
              min: 18,
              max: 99,
              labels: RangeLabels(
                currentRangeValues.start.round().toString(),
                currentRangeValues.end.round().toString(),
              ),
              onChanged: (val) {
                f.minAge = val.start;
                f.maxAge = val.end;
                bloc.add(FilterEvent(f, FilterAction.edit));
              },
            ),
            Text(currentRangeValues.end.round().toString()),
          ],
        ),
        Text('Country'),
        CountryPickerDropdown(
          itemBuilder: _buildDropdownItem,
          isExpanded: true,
          initialValue: f.country,
          onValuePicked: (country) {
            f.country = country.isoCode;
            bloc.add(FilterEvent(f, FilterAction.edit));
          },
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MyButton(
              onPressed: () {
                bloc.add(FilterEvent(f, FilterAction.save));
              },
              icon: Icons.done,
              text: 'Done',
            ),
          ],
        ),
      ],
    );
  }
}
