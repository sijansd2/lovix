import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lovix/bloc/home/home_bloc.dart';
import 'package:lovix/bloc/home/home_event.dart';
import 'package:lovix/bloc/profile/profile_bloc.dart';
import 'package:lovix/bloc/profile/profile_state.dart';
import 'package:lovix/models/profile.dart';

class MinsDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // ignore: close_sinks
    ProfileBloc _bloc = BlocProvider.of<ProfileBloc>(context);
    // ignore: close_sinks
    final _homeBloc = BlocProvider.of<HomeBloc>(context);
    return BlocBuilder<ProfileBloc, ProfileState>(
        bloc: _bloc,
        builder: (context, state) {
          Profile profile = state.profile;

          return Dialog(
            child: Padding(
              padding: EdgeInsets.all(10),
              child: SizedBox(
                width: 350,
                child: RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text:
                            'Currently you have ${profile.coins} and can last for x minutes, to get more you can purchase coins from ',
                        style: TextStyle(
                          color: Colors.black,
                        ),
                      ),
                      WidgetSpan(
                        child: InkWell(
                          onTap: () {
                            _homeBloc.add(OpenCoins());
                            Navigator.pop(context);
                          },
                          child: Text(
                            'Here',
                            style: TextStyle(
                              color: Colors.blue,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }
}
