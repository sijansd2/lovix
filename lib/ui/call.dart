import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:lovix/bloc/profile/profile_bloc.dart';
import 'package:lovix/bloc/profile/profile_event.dart';
import 'package:lovix/models/profile.dart';
import 'package:lovix/services/login_service.dart';
import 'package:lovix/ui/call_view.dart';
import 'package:lovix/utils/service_locator.dart';
import 'package:lovix/utils/signaling.dart';

// TODO make the call based on filter
class CallPage extends StatefulWidget {
  static const String ROUTE = 'call';

  @override
  _CallPageState createState() {
    return _CallPageState();
  }
}

class _CallPageState extends State<CallPage> {
  Signaling _signaling;
  List<dynamic> _peers;
  String _selfId = (locator<LoginService>()).auth.currentUser.uid;
  RTCVideoRenderer _localRenderer = RTCVideoRenderer();
  RTCVideoRenderer _remoteRenderer = RTCVideoRenderer();
  bool _inCalling = false;
  // ignore: close_sinks
  ProfileBloc profileBloc;
  Profile profile;

  @override
  initState() {
    profileBloc = BlocProvider.of<ProfileBloc>(context);
    profile = profileBloc.state.profile;

    super.initState();
    initRenderers();
    _connect();
    // Future.delayed(Duration.zero, () {
    //   _selfId = profile.uid;
    //   _connect();
    // });
  }

  @override
  void dispose() {
    profileBloc.add(ProfileEvent(profile));
    super.dispose();
  }

  initRenderers() async {
    await _localRenderer.initialize();
    await _remoteRenderer.initialize();
  }

  @override
  deactivate() {
    super.deactivate();
    if (_signaling != null) _signaling.close();
    _localRenderer.dispose();
    _remoteRenderer.dispose();
  }

  void _connect() async {
    if (_signaling == null) {
      _signaling = Signaling(_selfId)..connect();

      _signaling.onStateChange = (SignalingState state) {
        switch (state) {
          case SignalingState.CallStateNew:
            this.setState(() {
              _inCalling = true;
            });
            break;
          case SignalingState.CallStateBye:
            if (_signaling.lastCall != null)
              profile.addCall(_signaling.lastCall);
            this.setState(() {
              _localRenderer.srcObject = null;
              _remoteRenderer.srcObject = null;
              _inCalling = false;
            });
            break;
          case SignalingState.CallStateInvite:
          case SignalingState.CallStateConnected:
          case SignalingState.CallStateRinging:
          case SignalingState.ConnectionClosed:
          case SignalingState.ConnectionError:
          case SignalingState.ConnectionOpen:
            break;
        }
      };

      _signaling.onPeersUpdate = ((event) {
        this.setState(() {
          _selfId = event['self'];
          _peers = event['peers'];
        });
      });

      _signaling.onLocalStream = ((stream) {
        _localRenderer.srcObject = stream;
      });

      _signaling.onAddRemoteStream = ((stream) {
        _remoteRenderer.srcObject = stream;
      });

      _signaling.onRemoveRemoteStream = ((stream) {
        _remoteRenderer.srcObject = null;
      });
    }
  }

  _invitePeer(context, peerId, useScreen) async {
    if (_signaling != null && peerId != _selfId) {
      _signaling.invite(peerId, 'video', useScreen);
    }
  }

  _hangUp() {
    if (_signaling != null) {
      _signaling.bye();
    }
  }

  _switchCamera() {
    _signaling.switchCamera();
  }

  @override
  Widget build(BuildContext context) {
    if (_peers != null && _peers.length > 1 && !_inCalling) {
      for (final peer in _peers) {
        if (peer['id'].compareTo(_selfId) < 0) {
          _invitePeer(context, peer['id'], false);
          break;
        }
      }
    }

    return SafeArea(
      child: Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: SizedBox(
          width: 200.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              if (_inCalling)
                FloatingActionButton(
                  child: const Icon(Icons.switch_camera),
                  onPressed: _switchCamera,
                ),
              if (_inCalling)
                FloatingActionButton(
                  onPressed: _hangUp,
                  tooltip: 'Hangup',
                  child: Icon(Icons.call_end),
                  backgroundColor: Colors.pink,
                ),
              if (!_inCalling)
                FloatingActionButton(
                  child: const Icon(Icons.close),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
            ],
          ),
        ),
        body: _inCalling
            ? CallView(
                remoteRenderer: _remoteRenderer,
                localRenderer: _localRenderer,
              )
            : Center(
                child: CircularProgressIndicator(),
              ),
      ),
    );
  }
}
