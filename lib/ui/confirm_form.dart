import 'package:flutter/material.dart';

class ConfirmForm extends StatelessWidget {
  static const String agree = 'OK';
  static const String disagree = 'Nah';
  const ConfirmForm({
    Key key,
    t,
    @required this.message,
    this.confirmText = 'Okay',
    this.cancelText = 'Just Kidding',
    this.title = 'Sure?',
  }) : super(key: key);
  final String message;
  final String confirmText;
  final String cancelText;
  final String title;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(title),
      content: Text(message),
      actions: [
        RaisedButton(
          child: Text(confirmText),
          onPressed: () {
            Navigator.pop(context, agree);
          },
        ),
        RaisedButton(
          child: Text(cancelText),
          onPressed: () {
            Navigator.pop(context, disagree);
          },
        ),
      ],
    );
  }
}
