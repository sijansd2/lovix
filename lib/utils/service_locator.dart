import 'package:get_it/get_it.dart';
import 'package:lovix/services/coins_service.dart';
import 'package:lovix/services/filter_service.dart';
import 'package:lovix/services/login_service.dart';
import 'package:lovix/services/profile_service.dart';

GetIt locator = GetIt.instance;

setupServiceLocator() {
  locator.registerLazySingleton<LoginService>(() => LoginService());
  locator.registerLazySingleton<ProfileService>(() => ProfileService());
  locator.registerLazySingleton<FilterService>(() => FilterService());
  locator.registerLazySingleton<CoinsService>(() => CoinsService());
}
