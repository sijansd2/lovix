import 'package:lovix/models/profile.dart';
import 'package:lovix/services/profile_service.dart';
import 'package:lovix/utils/service_locator.dart';

class UserUtility {
  static final profileService = (locator<ProfileService>());

  static Future<Profile> getUser(String uid) {
    return profileService.getProfile(uid);
  }
}
