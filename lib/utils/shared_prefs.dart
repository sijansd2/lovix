import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefsUtility {
  static const String FCM_TOKEN = 'fcm_token';
  static SharedPreferences prefs;

  static Future<void> saveString(String key, String value) async {
    if (prefs == null) prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, value);
  }

  static Future<String> getString(String key) async {
    if (prefs == null) prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }
}
