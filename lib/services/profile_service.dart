import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:lovix/models/profile.dart';
import 'package:lovix/services/login_service.dart';
import 'package:lovix/utils/service_locator.dart';

class ProfileService {
  final auth = (locator<LoginService>()).auth;
  final store = FirebaseFirestore.instance;

  Profile getCurrentProfile() {
    Profile p = Profile.fromFirebaseUser(auth.currentUser);
    return p;
  }

  Future<Profile> saveProfile(Profile profile) async {
    await store.collection('users').doc(profile.uid).set(
          profile.doc,
          SetOptions(merge: true),
        );
    profile.saving = SavingState.normal;
    return profile;
  }

  Future<Profile> load(Profile p) async {
    DocumentSnapshot snap = await store.collection('users').doc(p.uid).get();
    if (snap.data() == null) {
      return saveProfile(p);
    }
    return Profile.fromDoc(snap.data());
  }

  Future<Profile> getProfile(String uid) async {
    DocumentSnapshot snap = await store.collection('users').doc(uid).get();
    if (snap.data() == null) {
      return Profile.fromDoc({'uid': uid});
    }
    return Profile.fromDoc(snap.data());
  }
}
