import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_auth_mocks/firebase_auth_mocks.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';

class LoginService {
  final FirebaseAuth auth = kDebugMode && kIsWeb
      ? MockFirebaseAuth(signedIn: true)
      : FirebaseAuth.instance;

  Future<void> siginInWithPhone(
    String number, {
    @required Function onCodeSent,
  }) async {
    return auth.verifyPhoneNumber(
      phoneNumber: number,
      verificationCompleted: (phoneAuthCredential) {
        print('credential: $phoneAuthCredential');
      },
      verificationFailed: (error) {
        print('error: $error');
      },
      codeSent: onCodeSent,
      codeAutoRetrievalTimeout: (verificationId) {
        print('timeout verId: $verificationId');
      },
    );
  }

  Future<UserCredential> signInWithGoogle() async {
    // Trigger the authentication flow
    final GoogleSignInAccount googleUser = await GoogleSignIn().signIn();

    // Obtain the auth details from the request
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;

    // Create a new credential
    final GoogleAuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    // Once signed in, return the UserCredential
    return await auth.signInWithCredential(credential);
  }

  Future<UserCredential> signInWithFacebook() async {
    final facebookLogin = FacebookLogin();
    final result = await facebookLogin.logIn(['email']);
    final OAuthCredential credential =
        FacebookAuthProvider.credential(result.accessToken.token);
    return await auth.signInWithCredential(credential);
  }

  Future<UserCredential> verifyCode(String code, String id) {
    PhoneAuthCredential phoneAuthCredential =
        PhoneAuthProvider.credential(verificationId: id, smsCode: code);
    return auth.signInWithCredential(phoneAuthCredential);
  }

  Future<void> logout() {
    return auth.signOut();
  }

  bool get isLoggedIn => auth.currentUser != null;
}
