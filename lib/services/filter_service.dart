import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:lovix/models/filter.dart';
import 'package:lovix/services/login_service.dart';
import 'package:lovix/utils/service_locator.dart';

class FilterService {
  final auth = (locator<LoginService>()).auth;
  final store = FirebaseFirestore.instance;

  Future<Filter> load() async {
    DocumentSnapshot snapshot =
        await store.collection(auth.currentUser.uid).doc('filter').get();
    return Filter.fromDoc(snapshot.data() ?? {});
  }

  Future<Filter> save(Filter f) async {
    await store
        .collection(auth.currentUser.uid)
        .doc('filter')
        .set(f.doc, SetOptions(merge: true));
    return f;
  }
}
