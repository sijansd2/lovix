import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:lovix/models/coin_package.dart';

class CoinsService {
  final _store = FirebaseFirestore.instance;

  Future<List<CoinPackage>> getPackages() async {
    final res = <CoinPackage>[];
    DocumentSnapshot snapshot =
        await _store.collection('coins').doc('packages').get();
    for (final v in snapshot.data()['1']) {
      res.add(CoinPackage(v['amount'].toDouble(), v['price'].toDouble()));
    }
    return res;
  }
}
