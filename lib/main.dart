import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lovix/bloc/filter/filter_bloc.dart';
import 'package:lovix/bloc/home/home_bloc.dart';
import 'package:lovix/bloc/login/login_bloc.dart';
import 'package:lovix/bloc/login/login_state.dart' as LoginState;
import 'package:lovix/bloc/profile/profile_bloc.dart';
import 'package:lovix/ui/call.dart';
import 'package:lovix/ui/home.dart';
import 'package:lovix/ui/login/login.dart';
import 'package:lovix/utils/service_locator.dart';

void main() {
  setupServiceLocator();
  runApp(MultiBlocProvider(providers: [
    BlocProvider<LoginBloc>(create: (_) => LoginBloc()),
    BlocProvider<ProfileBloc>(create: (_) => ProfileBloc()),
    BlocProvider<HomeBloc>(create: (_) => HomeBloc()),
    BlocProvider<FilterBloc>(create: (_) => FilterBloc()),
  ], child: MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'lovix',
      theme: ThemeData(
        brightness: Brightness.light,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        primaryTextTheme: TextTheme(
          headline5: TextStyle(color: Colors.black),
          headline6: TextStyle(color: Colors.black),
        ),
        appBarTheme: AppBarTheme(
          brightness: Brightness.light,
          color: Colors.white,
        ),
        scaffoldBackgroundColor: Colors.grey[200],
        primarySwatch: Colors.blue,
        accentColor: Colors.blue,
        canvasColor: Colors.white,
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
          backgroundColor: Colors.orange,
          selectedItemColor: Colors.white,
          unselectedItemColor: Colors.white60,
        ),
        toggleableActiveColor: Colors.orange,
      ),
      routes: {
        CallPage.ROUTE: (context) => CallPage(),
      },
      home: FutureBuilder(
        future: Firebase.initializeApp(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done)
            return BlocBuilder<LoginBloc, LoginState.LoginState>(
              bloc: BlocProvider.of<LoginBloc>(context),
              builder: (context, state) {
                FirebaseMessaging()
                  ..configure(onMessage: (Map<String, dynamic> message) async {
                    print('on message $message');
                  }, onResume: (Map<String, dynamic> message) async {
                    print('on resume $message');
                  }, onLaunch: (Map<String, dynamic> message) async {
                    print('on launch $message');
                  });
                if (state is LoginState.LoggedIn) return HomePage();
                return LoginPage();
              },
            );
          else
            return Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            );
        },
      ),
    );
  }
}
