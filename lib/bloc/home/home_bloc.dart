import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lovix/bloc/home/home_event.dart' as HomeEvent;
import 'package:lovix/bloc/home/home_state.dart' as HomeState;

class HomeBloc extends Bloc<HomeEvent.HomeEvent, HomeState.HomeState> {
  @override
  HomeState.HomeState get initialState => HomeState.Initial();
  @override
  Stream<HomeState.HomeState> mapEventToState(
      HomeEvent.HomeEvent event) async* {
    switch (event.runtimeType) {
      case HomeEvent.OpenProfile:
        yield HomeState.Profile();
        break;
      case HomeEvent.OpenCoins:
        yield HomeState.Coins();
        break;
      case HomeEvent.OpenSettings:
        yield HomeState.SettingsState();
        break;
      case HomeEvent.OpenHistorial:
        yield HomeState.HistorialState();
        break;
      case HomeEvent.OpenInitial:
      default:
        yield HomeState.Initial();
    }
  }
}
