import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lovix/bloc/filter/filter_event.dart';
import 'package:lovix/bloc/filter/filter_state.dart';
import 'package:lovix/models/filter.dart';
import 'package:lovix/services/filter_service.dart';
import 'package:lovix/utils/service_locator.dart';

class FilterBloc extends Bloc<FilterEvent, FilterState> {
  final FilterService filterService = locator<FilterService>();

  @override
  FilterState get initialState =>
      FilterState(Filter(), FilterCurrentState.initial);

  @override
  Stream<FilterState> mapEventToState(FilterEvent event) async* {
    Filter f = event.filter;
    switch (event.action) {
      case FilterAction.edit:
        yield (FilterState(f, FilterCurrentState.changed));
        break;
      case FilterAction.save:
        yield (FilterState(f, FilterCurrentState.initial));
        await filterService.save(f);
        yield (FilterState(f, FilterCurrentState.saved));
        break;
      case FilterAction.load:
        f = await filterService.load();
        yield (FilterState(f, FilterCurrentState.loaded));
        break;
    }
  }
}
