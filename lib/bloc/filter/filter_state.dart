import 'package:lovix/models/filter.dart';

enum FilterCurrentState { loaded, changed, initial, saved }

class FilterState {
  final Filter filter;
  final FilterCurrentState state;

  FilterState(this.filter, this.state);
}
