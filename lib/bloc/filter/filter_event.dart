import 'package:lovix/models/filter.dart';

enum FilterAction { edit, save, load }

class FilterEvent {
  final Filter filter;
  final FilterAction action;

  FilterEvent(this.filter, this.action);
}
