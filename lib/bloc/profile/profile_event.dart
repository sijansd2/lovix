import 'package:lovix/models/profile.dart';

class ProfileEvent {
  final Profile profile;

  ProfileEvent(this.profile);
}
