import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lovix/bloc/profile/profile_event.dart';
import 'package:lovix/bloc/profile/profile_state.dart';
import 'package:lovix/models/profile.dart';
import 'package:lovix/services/profile_service.dart';
import 'package:lovix/utils/service_locator.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  final ProfileService _profileService = locator<ProfileService>();

  @override
  ProfileState get initialState =>
      ProfileState(_profileService.getCurrentProfile());

  @override
  Stream<ProfileState> mapEventToState(ProfileEvent event) async* {
    Profile p = event.profile;
    p.saving = SavingState.saving;
    if (p.loaded) yield (ProfileState(p));
    if (!p.loaded)
      yield ProfileState(await _profileService.load(p));
    else
      yield ProfileState(await _profileService.saveProfile(p));
  }
}
