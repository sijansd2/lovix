import 'package:lovix/models/profile.dart';

class ProfileState {
  final Profile profile;

  ProfileState(this.profile);
}
