enum LoginAction {
  signWithGoogle,
  signWithPhone,
  codeSent,
  verifyPhone,
  signInWithFacebook,
  dismissMessage
}

class AuthEvent {}

class LoginEvent extends AuthEvent {
  final LoginAction action;
  final String data;
  final String verId;

  LoginEvent(this.action, {this.data, this.verId});
}

class LogoutEvent extends AuthEvent {}
