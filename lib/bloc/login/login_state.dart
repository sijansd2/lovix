class LoginState {}

class Loading extends LoginState {}

class Eror extends LoginState {
  final String _error;

  Eror(this._error);

  String get error => _error;
}

class Initial extends LoginState {}

class LoggedIn extends LoginState {}

class NeedCode extends LoginState {
  final String verId;
  NeedCode(this.verId);
}
