import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lovix/bloc/login/login_event.dart';
import 'package:lovix/bloc/login/login_state.dart' as LoginState;
import 'package:lovix/services/login_service.dart';
import 'package:lovix/utils/service_locator.dart';

class LoginBloc extends Bloc<AuthEvent, LoginState.LoginState> {
  final LoginService _loginService = locator<LoginService>();

  @override
  LoginState.LoginState get initialState =>
      _loginService.isLoggedIn ? LoginState.LoggedIn() : LoginState.Initial();

  @override
  Stream<LoginState.LoginState> mapEventToState(AuthEvent event) async* {
    yield LoginState.Loading();
    switch (event.runtimeType) {
      case LogoutEvent:
        await _loginService.logout();
        yield LoginState.Initial();
        break;
      case LoginEvent:
        LoginEvent ev = event as LoginEvent;
        switch (ev.action) {
          case LoginAction.signWithGoogle:
            try {
              final cred = await _loginService.signInWithGoogle();
              if (cred.user != null)
                yield LoginState.LoggedIn();
              else
                yield LoginState.Initial();
            } catch (ex) {
              yield LoginState.Eror(ex.toString());
            }
            break;
          case LoginAction.codeSent:
            yield LoginState.NeedCode(ev.data);
            break;
          case LoginAction.signWithPhone:
            try {
              String number = ev.data;
              await _loginService.siginInWithPhone(
                number,
                onCodeSent: codeSent,
              );
            } catch (ex) {
              yield LoginState.Eror(ex.toString());
            }
            break;
          case LoginAction.verifyPhone:
            await _loginService.verifyCode(ev.data, ev.verId);
            yield LoginState.LoggedIn();
            break;
          case LoginAction.signInWithFacebook:
            try {
              final cred = await _loginService.signInWithFacebook();
              if (cred.user != null)
                yield LoginState.LoggedIn();
              else
                yield LoginState.Initial();
            } catch (ex) {
              yield LoginState.Eror(ex.toString());
            }
            break;
          case LoginAction.dismissMessage:
            yield LoginState.Initial();
            break;
        }
        break;
    }
  }

  void codeSent(String s, int i) {
    this.add(LoginEvent(LoginAction.codeSent, data: s));
  }
}
