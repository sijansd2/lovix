import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

enum SavingState { normal, saving, saved }

abstract class Copyable<T> {
  T copy();
  T copyWith();
}

class Profile implements Copyable<Profile> {
  static const double INITIAL_COINS = 400;
  String name;
  String photoUrl;
  String uid;
  int age;
  String about;
  String job;
  bool loaded = false;
  SavingState saving = SavingState.normal;
  double coins;
  List<Call> history;

  Profile(this.uid, this.name);

  factory Profile.fromFirebaseUser(User user) {
    Profile profile = Profile(user.uid, user.displayName);
    if (user.photoURL != null) profile.photoUrl = user.photoURL;
    return profile;
  }

  factory Profile.fromDoc(Map<String, dynamic> doc) {
    Profile profile = Profile(doc['uid'], doc['name'] ?? 'Unknown');
    profile.loaded = true;
    if (doc['photo'] != null) profile.photoUrl = doc['photo'];
    if (doc['age'] != null) profile.age = doc['age'];
    if (doc['about'] != null) profile.about = doc['about'];
    if (doc['job'] != null) profile.job = doc['job'];
    profile.coins = doc['coins'] ?? INITIAL_COINS;
    profile.history = [];
    if (doc['history_calls'] != null) {
      for (var val in doc['history_calls']) {
        profile.history.add(Call.fromDoc(val));
      }
    }
    return profile;
  }

  Map<String, dynamic> get doc => {
        'name': name,
        'photo': photoUrl,
        'uid': uid,
        'age': age,
        'about': about,
        'job': job,
        'coins': coins,
        'history_calls': history.map((e) => e.doc).toList(),
      };

  @override
  String toString() {
    return doc.toString();
  }

  void addCall(Call call) {
    history.add(call);
  }

  @override
  Profile copy() {
    Profile p = Profile.fromDoc(doc);
    return p;
  }

  @override
  Profile copyWith({
    bool isLoaded = false,
    SavingState savingState = SavingState.normal,
  }) {
    Profile p = Profile.fromDoc(doc);
    p.loaded = isLoaded;
    p.saving = savingState;
    return p;
  }
}

class Call {
  final String otherUid;
  DateTime start;
  DateTime end;

  factory Call.fromDoc(Map<String, dynamic> doc) {
    Call call = Call(doc['other']);
    call.start = (doc['start'] as Timestamp)?.toDate();
    call.end = (doc['end'] as Timestamp)?.toDate();
    return call;
  }

  Call(this.otherUid) {
    start = DateTime.now();
  }

  void callFinished() {
    end = DateTime.now();
  }

  Map<String, dynamic> get doc => {
        'other': otherUid,
        'start': start,
        'end': end,
      };
}
