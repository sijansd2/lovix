class CoinPackage {
  final double amount;
  final double price;

  CoinPackage(this.amount, this.price);
}
