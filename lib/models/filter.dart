class Filter {
  static const int MALE = 0;
  static const int FEMALE = 1;
  static const double DEF_MIN_AGE = 18;
  static const double DEF_MAX_AGE = 99;
  static const String DEF_COUNTRY = 'US';

  int gender = MALE;
  double minAge = DEF_MIN_AGE;
  double maxAge = DEF_MAX_AGE;

  String country = DEF_COUNTRY;

  factory Filter.fromDoc(Map<String, dynamic> d) {
    Filter filter = Filter();
    filter.gender = d['gender'] ?? MALE;
    filter.minAge = d['minAge'] ?? DEF_MIN_AGE;
    filter.maxAge = d['maxAge'] ?? DEF_MAX_AGE;
    filter.country = d['country'] ?? DEF_COUNTRY;
    return filter;
  }
  Filter();

  Map<String, dynamic> get doc => {
        'gender': gender,
        'minAge': minAge,
        'maxAge': maxAge,
        'country': country,
      };
}
